<?php include("includes/header.php"); ?>

<!-- Jumbotron -->
<section class="jumbotron jumbotron-fluid jumbotron-overlay jumbotron-under-nav bg-cover m-b-0">
    <figure class="bg-cover__img">
        <img class="jumbotron-img" alt="FPO" src="/build/images/img-your-health.jpg" />
    </figure>

    <div class="jumbotron-overlay__bd">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-10 col-lg-8">
                    <h1 class="jumbotron-title display-1">Your Health</h1>
                    <p class="p-lg">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <a class="btn btn-info" href="#">Lorem Ipsum Dolar</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Blog Preview Cards -->
<section class="container m-t-3">
    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <article class="card card-post-preview">
                <img class="card-img-top img-fluid img-rounded" src="/build/images/fpo-thumb1.jpg" alt="FPO blog post thumbnail" />
                <div class="card-block">
                    <div class="card-subtitle h6 shape-inline">
                        <svg width="16" height="19"><use xlink:href="#shape-bookmark"></use></svg>
                        <span>August 8, 2016</span>
                    </div>
                    <a href="#" class="card-title h3">Sealing the Perfect Smile!</a>
                    <p class="card-text text-sm">Delta Dental is funding an initiative to provide children with dental sealants.</p>
                    <footer class="">
                        <a class="link-more text-sm font-weight-medium shape-inline" href="#">
                            <span>Read More</span>
                            <svg width="13" height="13"><use xlink:href="#shape-circle-arrow-right"></use></svg>
                        </a>
                    </footer>
                </div>
            </article>
        </div>
        <div class="col-xs-12 col-sm-4">
            <article class="card card-post-preview">
                <img class="card-img-top img-fluid img-rounded" src="/build/images/fpo-thumb2.jpg" alt="FPO blog post thumbnail" />
                <div class="card-block">
                    <div class="card-subtitle h6 shape-inline">
                        <svg width="16" height="19"><use xlink:href="#shape-bookmark"></use></svg>
                        <span>July 30, 2016</span>
                    </div>
                    <a href="#" class="card-title h3">Major Life Changes &amp; Your Dental Insurance</a>
                    <p class="card-text text-sm">What you need to know about how major changes in your life affect your dental care.</p>
                    <footer class="">
                        <a class="link-more text-sm font-weight-medium shape-inline" href="#">
                            <span>Read More</span>
                            <svg width="13" height="13"><use xlink:href="#shape-circle-arrow-right"></use></svg>
                        </a>
                    </footer>
                </div>
            </article>
        </div>
        <div class="col-xs-12 col-sm-4">
            <article class="card card-post-preview">
                <img class="card-img-top img-fluid img-rounded" src="/build/images/fpo-thumb3.jpg" alt="FPO blog post thumbnail" />
                <div class="card-block">
                    <div class="card-subtitle h6 shape-inline">
                        <svg width="16" height="19"><use xlink:href="#shape-bookmark"></use></svg>
                        <span>July 1, 2016</span>
                    </div>
                    <a href="#" class="card-title h3">Don't Toss the Floss</a>
                    <p class="card-text text-sm">Recent studies have questioned the necessity of flossing, but here’s the truth.</p>
                    <footer class="">
                        <a class="link-more text-sm font-weight-medium shape-inline" href="#">
                            <span>Read More</span>
                            <svg width="13" height="13"><use xlink:href="#shape-circle-arrow-right"></use></svg>
                        </a>
                    </footer>
                </div>
            </article>
        </div>
    </div>
    <div class="hr-btn clearfix m-b-4">
        <a class="btn btn-info pull-xs-right" href="#">View All</a>
    </div>
</section>

<!-- Cards -->
<div class="container">
    <section class="card-deck-wrapper">
        <div class="card-deck">
            <div class="card card-rounded card-support card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/kid.svg" />
                    <div class="card-subtitle h6">Oral Health for Kids</div>
                    <div class="card-title h3">Good Oral Health Starts Even Before the First Tooth</div>
                    <p class="card-text p-lg">Learn more about youth oral health.</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
            <div class="card card-rounded card-primary card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/checkmark.svg" />
                    <div class="card-subtitle h6">Learn More</div>
                    <div class="card-title h3">Assess Your Oral Health</div>
                    <p class="card-text p-lg">Access our oral health content and video libraries, read grin! magazine, and submit your questions to our dental director.</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
        </div>
        <div class="card-deck">
            <div class="card card-rounded card-info card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/book.svg" />
                    <div class="card-subtitle h6">Oral Health Library</div>
                    <div class="card-title h3">Need More Oral Health Tools?</div>
                    <p class="card-text p-lg">Access our oral health content and video libraries, read grin! magazine, and submit your questions to our dental director.</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
            <div class="card card-rounded card-secondary card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/magazine.svg" />
                    <div class="card-subtitle h6">Read Our Publication</div>
                    <div class="card-title h3">grin! Magazine</div>
                    <p class="card-text p-lg">News, notes and entertainment to keep your smile healthy and happy.</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Fluid Card -->
<div class="container-fluid card card-fluid card-inverse card-primary card-bg-pattern" id="shop-plans">
    <div class="row">
        <div class="col-xs-12 col-md-6 bg-cover--after-sm">
            <figure class="bg-cover__img">
                <img class="" alt="FPO" src="/build/images/img-community-cta.jpg" />
            </figure>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="card-block">
                <img class="card-icon card-icon-logo" src="/build/images/svgs/logo-primary-white.svg" alt="Delta Dental logo" />
                <div class="card-subtitle h6">Delta Video Library</div>
                <div class="card-title h3">Learn More About Your Dental Insurance by Tuning into our Video Library</div>
                <p class="card-text p-lg">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                <a class="btn btn-card" href="#">Browse Videos</a>
            </div>
        </div>
    </div>
</div>

<?php include("includes/footer.php"); ?>
