// ---------------------------------------------------------------------
// Global JavaScript
// Authors: Wallace Co.
// Contributions: Jamie Paul
// ---------------------------------------------------------------------

var WALLACE = WALLACE || {};

(function($, APP) {

    $(function() {
        APP.BrowserDeviceDetection.init();
        APP.ScrollTo.init();
        APP.ClickFunction.init();
        APP.ClickGroup.init();
        APP.DataFilter.init();
    });

// ---------------------------------------------------------------------
// Browser and Feature Detection
// ---------------------------------------------------------------------

APP.BrowserDeviceDetection = {
    userAgent: undefined,
    $html: undefined,

    init: function() {
        APP.Features = APP.Features || {};
        this.userAgent = navigator.userAgent.toLowerCase();
        this.$html = $('html');
        this.detectJs();
        this.noTouch();
        this.isTouch();
        this.isNewIE();
        this.isIE10();
        this.isIE9();
        this.isIE8();
        this.isIE();
        this.isIPad();
        this.isAndroid();
    },

    detectJs: function() {
        $('body').removeClass('no-js');
        $(document).ready(function() {
            $('body').addClass('page-loaded');
        });
    },

    noTouch: function() {
        if ( ! ('ontouchstart' in window) ) {
            APP.Features.noTouch = false;
            this.$html.addClass('noTouch');
            return;
        }
        APP.Features.noTouch = false;
    },

    isTouch: function() {
        if ( 'ontouchstart' in window ) {
            APP.Features.isTouch = false;
            this.$html.addClass('isTouch');
            return;
        }
        APP.Features.isTouch = false;
    },

    isNewIE: function() {
        if (document.documentMode || /Edge/.test(navigator.userAgent)) {
            if(navigator.appVersion.indexOf('Trident') === -1) {
                this.$html.addClass('isIE isEDGE');
            } else {
                this.$html.addClass('isIE isIE11');
            }
            return;
        }

        APP.Features.isNewIE = false;
    },

    isIE10: function() {
        if( navigator.appVersion.indexOf("MSIE 10") !== -1 ) {
            this.$html.addClass('isIE10');
            APP.Features.isIE10 = true;
            return;
        }
        APP.Features.isIE10 = false;
    },

    isIE9: function() {
        if( navigator.appVersion.indexOf("MSIE 9") !== -1 ) {
            this.$html.addClass('isIE9');
            APP.Features.isIE9 = true;
            return;
        }
        APP.Features.isIE9 = false;
    },

    isIE8: function() {
        if( navigator.appVersion.indexOf("MSIE 8") !== -1 ) {
            this.$html.addClass('isIE8');
            APP.Features.isIE8 = true;
            return;
        }
        APP.Features.isIE9 = false;
    },

    isIE: function() {
        if( navigator.appVersion.indexOf("MSIE") !== -1 ) {
            this.$html.addClass('isIE');
            APP.Features.isIE = true;
            return;
        }
        APP.Features.isIE = false;
    },

    isIPad: function() {
        if( this.userAgent.indexOf("ipad") > -1 ) {
            this.$html.addClass('isIpad');
            APP.Features.isIPad = true;
            return;
        }
        APP.Features.isIPad = false;
    },

    isAndroid: function() {
        if( this.userAgent.indexOf("android") > -1 ) {
            this.$html.addClass('isAndroid');
            APP.Features.isAndroid = true;
            return;
        }
        APP.Features.isAndroid = false;
    }
};


// ---------------------------------------------------------------------
// Scroll to
// Used for smooth scrolling to elements
// ---------------------------------------------------------------------

APP.ScrollTo = {

    init: function() {
        var $scrollTo = $('*[data-scroll-to]');
        if( ! $scrollTo.length ) {
            return;
        }
        this.$scrollTo = $scrollTo;
        this.bind();
    },

    bind: function() {

        $('*[data-scroll-to]').on('click touchstart:not(touchmove)', function() {

            var $trigger = $(this).attr('data-scroll-to');
            var $target = $("#" + $trigger);

            var $scrollSpeed = 1000;
            var $offset = 0;

            if( $(this).attr('data-scroll-speed') ) {
                $scrollSpeed = $(this).attr('data-scroll-speed');
            }

            if( $(this).attr('data-scroll-offset') ) {
                $offset = $(this).attr('data-scroll-offset');
            }

            $('html, body').animate({
                scrollTop: $target.offset().top - $offset
            }, $scrollSpeed);
        });


    }
};


// ---------------------------------------------------------------------
// Click Functions
// Uses data-target & data-addClass to target modules & optionally add
// a class to the body
// ---------------------------------------------------------------------

APP.ClickFunction = {

    init: function() {
        var $clickFunction = $('*[data-click-target]');
        if( ! $clickFunction.length ) {
            return;
        }
        this.$clickFunction = $clickFunction;
        this.bind();
    },

    bind: function() {

        $('*[data-click-target]').on('click touchstart:not(touchmove)', function() {

            var $trigger = $(this).attr('data-click-target');
            var $bodyCls = $(this).attr('data-click-bodyClass');
            var $optCls = $(this).attr('data-click-class');
            var $target = $("#" + $trigger);

            // Check for custom class
            if( $(this).attr('data-click-class') ) {
                if( $target.hasClass($optCls) ) {
                    $target.removeClass($optCls);
                } else {
                   $target.addClass($optCls);
                }
            } else {
                if( $target.hasClass('is-active') ) {
                    $target.removeClass('is-active');
                } else {
                   $target.addClass('is-active');
                }
            }

            // Check for additional body class
            if( $(this).attr('data-click-bodyClass') ) {
                if( $('body').hasClass($bodyCls) ) {
                    $('body').removeClass($bodyCls);
                } else {
                   $('body').addClass($bodyCls);
                }
            }
        });
    }
};

// ---------------------------------------------------------------------
// Click Group
// Used for adding/ removing active classes on linked elements
// ---------------------------------------------------------------------

APP.ClickGroup = {

    init: function() {
        var $clickGroup = $('*[data-click-group]');
        if( ! $clickGroup.length ) {
            return;
        }
        this.$clickGroup = $clickGroup;
        this.bind();
    },

    bind: function() {

        $('*[data-click-group]').on('click touchstart:not(touchmove)', function() {

            var $group = $(this).attr('data-click-group');

            $('*[data-click-group=' + $group + ']').each(function() {
                $(this).removeClass('is-active');
            });

            $(this).addClass('is-active');

        });
    }
};

// -------------------------------------------------------------------------
// Data Filter
// Used for adding/removing active and hidden classes on associated elements
// -------------------------------------------------------------------------

APP.DataFilter = {
    init: function() {
        var $filterList = $('.js-filter-list');
        if( ! $filterList.length ) {
            return;
        }
        this.$filterList = $filterList;
        this.bind();
    },

    bind: function() {

        $('.js-filter-list > *').on('click touchstart:not(touchmove)', function() {

            var $filterItem = $(this).attr('data-filter');

            $('.js-filter-items').addClass('is-loading');
            $('.js-filter-list > *').removeClass('is-active');
            $(this).addClass('is-active');

            setTimeout(function() {
                $('.js-filter-items').removeClass('is-loading');

                $('.js-filter-items > *').each(function() {
                    // if( $(this).hasClass($filterItem) ) {
                    if( $filterItem === $(this).data('filter-item') ) {
                        $(this).removeClass('is-hidden');
                    } else {
                       $(this).addClass('is-hidden');
                    }
                });
            }, 1200);
        });

    }
};


}(jQuery, WALLACE));
