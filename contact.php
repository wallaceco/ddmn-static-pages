<?php include("includes/header.php"); ?>

<!-- Jumbotron -->
<section class="jumbotron jumbotron-fluid jumbotron-overlay jumbotron-under-nav bg-cover m-b-0">
    <figure class="bg-cover__img">
        <img class="jumbotron-img" alt="FPO" src="/build/images/img-contact.jpg" />
    </figure>

    <div class="jumbotron-overlay__bd">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-10 col-lg-8">
                    <h1 class="jumbotron-title display-1"><span class="font-weight-bold">Contact Us</span></h1>
                    <p class="p-lg">Delta Dental works to ensure you receive the best possible service. If you have any questions or comments, please contact us!</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <!-- Collapse Triggers -->
    <div class="bg-pattern bg-secondary p-t-3 p-b-3">
        <div class="container">
            <div class="row flex-items-md-center flex-items-xs-middle">
                <div class="col-xs-12 col-sm-2 col-lg-1">
                    <h4 class="text-white text-xs-center text-sm-right m-b-0">I am</h4>
                </div>
                <div class="col-xs-12 col-sm-10 col-lg-8">
                    <div id="dropdownFilter" class="dropdown" data-click-target="dropdownFilter">
                        <div class="dropdown__icon">
                            <svg width="18px" height="10px"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-arrow-dd"></use></svg>
                        </div>
                        <ul class="dropdown__list rounded h4 filter-list js-filter-list">
                            <li data-filter="dd1" class="is-active"><span>a current Delta Dental of Minnesota member</span></li>
                            <li data-filter="dd2"><span>shopping for a dental plan for myself or for my family</span></li>
                            <li data-filter="dd3"><span>an employer managing my group plan</span></li>
                            <li data-filter="dd4"><span>shopping for a dental plan for my employees</span></li>
                            <li data-filter="dd5"><span>an agent</span></li>
                            <li data-filter="dd6"><span>a dental provider</span></li>
                            <li data-filter="dd7"><span>looking for your corporate headquarters</span></li>
                            <li data-filter="dd8"><span>reporting fraud</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row flex-items-md-center">
            <div class="col-xs-12 col-lg-9 filter-items js-filter-items">
                <!-- I am a current Delta Dental of Minnesota member -->
                <div class="m-y-3 user-content" data-filter-item="dd1">
                    <h3>Please use the information on the back of your ID card to call customer service or to mail a claim.</h3>
                    <br />
                    <br />
                    <h6>If you get your dental insurance from your employer</h6>
                    <p>If you do not have an ID card, please call us at <strong>1-800-553-9536</strong>. Lines are open between 7:00 a.m. to 7:00 p.m. Central Standard Time.</p>
                    <blockquote>
                        <h6>Mailing address for appeals</h6>
                        <p>
                            Delta Dental of Minnesota<br />
                            PO Box 9304<br />
                            Minneapolis, MN 55440-9304
                        </p>
                    </blockquote>
                    <br />
                    <h6>If you get your dental insurance directly from Delta Dental</h6>
                    <p>If you do not have an ID card, please call us at <strong>1-855-643-3582</strong> or log-in to the secure member portal by visiting <a href="https://www.DeltaDentalMN.org/myaccount">www.DeltaDentalMN.org/myaccount</a>.</p>
                    <blockquote>
                        <h6>Mailing address for appeals</h6>
                        <p>
                            Delta Dental of Minnesota<br />
                            Attn: Professional Services Appeals and Grievances<br />
                            PO Box 30416<br />
                            Lansing, MI 48909
                        </p>
                    </blockquote>
                    <br />
                    <h6>If you are a Member of the Minnesota Health Care Programs or CivicSmiles</h6>
                    <p>Contact customer service at <strong>1-800-774-9049.</strong></p>
                </div>
                <!-- I am shopping for a dental plan for myself or for my family -->
                <div class="m-y-3 user-content is-hidden" data-filter-item="dd2">
                    <p>To speak with a licensed agent that can help you navigate the best plan to meet your unique needs and budget, please call us at <strong>1-866-764-5350</strong></p>
                </div>
                <!-- I am an employer managing my group plan -->
                <div class="m-y-3 user-content is-hidden" data-filter-item="dd3">
                    <p>For billing assistance, forms and additional support materials, please be sure to visit our <a href="https://www.deltadentalmn.org/resources/employer-resources/">employer resources</a> webpage.</p>
                    <blockquote>
                        <h6>Group billing address</h6>
                        <p>
                            Delta Dental of Minnesota<br />
                            Attn: Group Billing<br />
                            PO Box 9304<br />
                            Minneapolis, MN 55440-9304
                        </p>
                    </blockquote>
                </div>
                <!-- I am shopping for a dental plan for my employees -->
                <div class="m-y-3 user-content is-hidden" data-filter-item="dd4">
                    <p>For small group sales (up to 50 eligible employees) please contact your broker or Delta Dental Connect<sup>SM</sup> at <strong>1-800-906-5250</strong> or locally at <strong>651-406-5920</strong></p>
                    <p>All other businesses, please contact your broker or our sales team at <strong>1-877-268-3384 ext. 3236</strong></p>
                </div>
                <!-- I am an agent -->
                <div class="m-y-3 user-content is-hidden" data-filter-item="dd5">
                    <p>Looking to get appointed with Delta Dental of Minnesota? Appointment paperwork can be completed online at: <a href="//www.deltadentalmn.org/appointment">www.deltadentalmn.org/appointment</a>.</p>
                    <p>Should you need additional assistance, with your appointment, with your dedicated broker web link or with commissions please contact us at <a href="mailto:DDMNBroker@DeltaDentalMN.org">DDMNBroker@DeltaDentalMN.org</a>.</p>
                    <br />
                    <br />
                    <h3>Sales</h3>
                    <br />
                    <h6>Individual and Family Sales Support:</h6>
                    <p>For Individual or Family Plan sales assistance, please contact us at <strong>1-866-764-5350</strong> or <a href="mailto:Sales@DeltaDentalMN.org">Sales@DeltaDentalMN.org</a></p>
                    <br />
                    <h6>Group Sales Support:</h6>
                    <p>For small group sales (up to 50 eligible employees) contact Delta Dental Connect<sup>SM</sup>:</p>
                    <p>Phone: <strong>1-800-906-5250</strong> or locally at <strong>651-406-5920</strong><br />Email: <a href="mailto:deltadentalconnect@deltadentalmnadmin.org">deltadentalconnect@deltadentalmnadmin.org</a></p>
                    <br />
                    <br />
                    <h3>Enrollment</h3>
                    <br />
                    <h6>For Group Enrollment, Premium Billing or Other Questions:</h6>
                    <p>Please call our Employer Services team at <strong>1-866-318-9449</strong> or locally at <strong>651-994-5300</strong> or fax <strong>651-994-5414</strong></p>
                    <p>All other businesses, please contact our sales team at <strong>1-877-268-3384 ext. 3236</strong> or fax <strong>651-994-5414</strong></p>
                    <blockquote>
                        <h6>Group Enrollment Mailing Address</h6>
                        <p>
                            Delta Dental of Minnesota<br />
                            Attn: Enrollment Department<br />
                            PO Box 330<br />
                            Minneapolis, MN 55440-0330
                        </p>
                    </blockquote>
                    <br />
                    <h6>For Individual and Family Plan Enrollment, Premium Billing or Other Questions:</h6>
                    <p>Please call our customer service team at <strong>855-643-3582</strong></p>
                    <blockquote>
                        <h6>Individual and Family Plan Enrollment Mailing Address</h6>
                        <p>
                            Delta Dental of Minnesota<br />
                            Attn: Enrollment Department<br />
                            PO Box 74008400<br />
                            Chicago, IL 60674-8400
                        </p>
                    </blockquote>
                </div>
                <!-- I am a dental provider -->
                <div class="m-y-3 user-content is-hidden" data-filter-item="dd6">
                    <p>For member eligibility, benefits and claim information, please call the number on the back of the member's ID card.</p>
                    <p>For all other questions, call <strong>1-800-328-1188 ext. 4170</strong></p>
                </div>
                <!-- I am looking for your corporate headquarters -->
                <div class="m-y-3 user-content is-hidden" data-filter-item="dd7">
                    <h3>Looking for headquarters? Here's our info.</h3>
                    <br />
                    <br />
                    <h6>Contact Us</h6>
                    <p>Please call us at <strong>1-877-268-3384</strong></p>
                    <blockquote>
                        <h6>Corporate Address</h6>
                        <p>
                            Delta Dental of Minnesota<br />
                            500 Washington Avenue South<br />
                            Suite 2060<br />
                            Minneapolis, MN 55415
                        </p>
                    </blockquote>
                    <br />
                    <p>News Media Inquiries: <strong>612-224-3295</strong></p>
                    <p><a href="https://www.deltadentalmn.org/wp-content/uploads/foundation_contact.pdf">Contact</a> Delta Dental of Minnesota Foundation</p>
                    <br />
                    <a href="https://www.google.com/maps/dir/''/Delta+Dental+of+Minnesota,+500+Washington+Ave+S,+Minneapolis,+55415/@44.9789424,-93.3288604">
                        <img class="img-fluid" src="/build/images/map-img.png" alt="Delta Dental of Minnesota Corporate Headquarters map" />
                    </a>
                </div>
                <!-- I am reporting fraud -->
                <div class="m-y-3 user-content is-hidden" data-filter-item="dd8">
                    <p>Phone: <strong>612-224-3277</strong></p>
                    <p>Fax: <strong>612-351-5196</strong></p>
                    <p>Email: <a href="mailto:reportfraud@deltadentalmn.org">reportfraud@deltadentalmn.org</a></p>
                    <br />
                    <blockquote>
                        <h6>Mailing Address</h6>
                        <p>
                            Delta Dental of Minnesota<br />
                            Attn: Report Fraud<br />
                            500 Washington Avenue South<br />
                            Suite 2060<br />
                            Minneapolis, MN 55415
                        </p>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include("includes/footer.php"); ?>
