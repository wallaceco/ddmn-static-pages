<?php include("includes/header.php"); ?>

<!-- Jumbotron -->
<section class="jumbotron jumbotron-fluid jumbotron-overlay jumbotron-under-nav bg-cover">
    <figure class="bg-cover__img">
        <img class="jumbotron-img" alt="FPO" src="/build/images/img-members.jpg" />
    </figure>

    <div class="jumbotron-overlay__bd">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-10 col-lg-8">
                    <h6 class="jumbotron-subtitle h6">Members</h6>
                    <h1 class="jumbotron-title display-1">Manage Your <span class="font-weight-bold">Dental Benefits</span></h1>
                    <p class="p-lg">Sign in to manage your dental benefits summary and claims history, order replacement ID cards and see other exclusive membership benefits.</p>
                    <a class="btn btn-info" href="#">Access Member Portal</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Cards -->
<div class="container">
    <section class="card-deck-wrapper">
        <div class="card-deck">
            <div class="card card-rounded card-support card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/user.svg" />
                    <div class="card-subtitle h6">Account</div>
                    <div class="card-title h3">Manage Your Account</div>
                    <p class="card-text p-lg">Need to order a new ID card or make changes to your account? Access forms and additional resources.</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
            <div class="card card-rounded card-secondary card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/pin-with-tooth.svg" />
                    <div class="card-subtitle h6">Find A Dentist</div>
                    <div class="card-title h3">Find a Dentist Near You</div>
                    <p class="card-text p-lg">With Delta Dental’s extensive dental networks, you’re guaranteed to find a dentist near you.</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
        </div>
        <div class="card-deck">
            <div class="card card-rounded card-info card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/question-mark.svg" />
                    <div class="card-subtitle h6">Dental Insurance 101</div>
                    <div class="card-title h3">Need Help Understanding Your Insurance?</div>
                    <p class="card-text p-lg">We are here to help.</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
            <div class="card card-rounded card-primary card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/heart-with-tooth.svg" />
                    <div class="card-subtitle h6">Your Health</div>
                    <div class="card-title h3">A Healthy Mouth is Part of a Healthy Life</div>
                    <p class="card-text p-lg">Take advantage of a variety of oral health tips, resources and information to improve your oral and overall health.</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include("includes/footer.php"); ?>
