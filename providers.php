<?php include("includes/header.php"); ?>

<!-- Jumbotron -->
<section class="jumbotron jumbotron-fluid jumbotron-overlay jumbotron-under-nav bg-cover">
    <figure class="bg-cover__img">
        <img class="jumbotron-img" alt="FPO" src="/build/images/img-providers.jpg" />
    </figure>

    <div class="jumbotron-overlay__bd">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-10 col-lg-8">
                    <h6 class="jumbotron-subtitle h6">Providers</h6>
                    <h1 class="jumbotron-title display-1">Tools to Better <br /><span class="font-weight-bold">Serve Your Patients</span></h1>
                    <a class="btn btn-info" href="#">Access Provider Portal</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Cards -->
<div class="container">
    <section class="card-deck-wrapper">
        <div class="card-deck">
            <div class="card card-rounded card-support card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/book.svg" />
                    <div class="card-subtitle h6">Provider Resources</div>
                    <div class="card-title h3">Forms &amp; Publications</div>
                    <p class="card-text p-lg">Access commonly used forms and resources for Delta Dental network providers.</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
            <div class="card card-rounded card-primary card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/logo-mark-white.svg" />
                    <div class="card-subtitle h6">Join Our Networks</div>
                    <div class="card-title h3">Become a Network Provider</div>
                    <p class="card-text p-lg">Interested in becoming a Delta Dental network provider?</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
        </div>
        <div class="card-deck">
            <div class="card card-rounded card-info card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/calendar.svg" />
                    <div class="card-subtitle h6">Event Calendar</div>
                    <div class="card-title h3">Provider Events Calendar</div>
                    <p class="card-text p-lg">Stay up to date on upcoming events by visiting our provider event calendar.</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
            <div class="card card-rounded card-secondary card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/tools.svg" />
                    <div class="card-subtitle h6">Health Resources for Providers</div>
                    <div class="card-title h3">Need Help Understanding Your Insurance?</div>
                    <p class="card-text p-lg">Access health resources to help keep your patients healthy.</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include("includes/footer.php"); ?>
