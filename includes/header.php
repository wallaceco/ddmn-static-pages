<!DOCTYPE html>
<html lang="en">
<head>
<!-- META DATA -->
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1"> -->
<!--[if IE]><meta http-equiv="cleartype" content="on" /><![endif]-->

<title>DDMN | Static Pages</title>

<!-- ICONS -->
<link rel="icon" href="http://www.deltadentalmn.org/wp-content/themes/deltamn/favicon.png" type="image/png" />
<link rel="shortcut icon" href="http://www.deltadentalmn.org/wp-content/themes/deltamn/favicon.ico" />
<!--[if IE]>
<link rel="shortcut icon" href="http://www.deltadentalmn.org/wp-content/themes/deltamn/favicon.ico">
<![endif]-->

<link rel="stylesheet" type="text/css" href="https://cloud.typography.com/6549574/658108/css/fonts.css" />

<!-- CSS -->
<link rel="stylesheet" media="screen" href="build/css/production.min.css" />

</head>
<body class="no-js" id="top">
<div class="is-hidden">
    <?php include("build/images/svgs/svgs.svg"); ?>
</div>

<header id="collapseHeader" class="site-header panel">
    <div class="container first">
        <!-- Nav Toggle -->
        <a href="#collapseNav" class="site-header__nav-toggle navbar-toggler hidden-md-up flex-vh-center" data-toggle="collapse" data-parent="#collapseHeader" aria-expanded="false" aria-controls="collapseNav">
            <svg fill="#43b02a" width="20" height="19"><use xlink:href="#shape-menu"></use></svg>
        </a>

        <!-- Logo -->
        <a href="/index.php" class="site-header__brand text-xs-center text-md-left">
            <img src="build/images/svgs/logo-w-text.svg" alt="Delta Dental" />
            <h1 class="sr-only">Delta Dental of Minnesota</h1>
        </a>

        <!-- Search Toggle -->
        <a href="#collapseSearch" class="site-header__search-toggle hidden-md-up flex-vh-center" data-toggle="collapse" data-parent="#collapseHeader" aria-expanded="false" aria-controls="collapseSearch">
            <svg fill="#43b02a" width="24" height="24"><use xlink:href="#shape-search"></use></svg>
        </a>
    </div>

    <div class="mobile-nav collapse-nav collapse" data-click-group="collapse-header" id="collapseNav">
        <!-- Navbar-Primary -->
        <div class="container clearfix">
            <nav class="navbar-primary pull-md-right" role="navigation">
                <ul class="nav nav-primary">
                    <li class="nav-item">
                        <a class="nav-link" href="/employers.php">Employers <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/providers.php">Providers</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/agents.php">Agents</a>
                    </li>
                </ul>
            </nav>
        </div>

        <!-- Navbary-Secondary -->
        <nav class="navbar navbar-secondary" role="navigation">
            <div class="container clearfix">
                <ul class="nav nav-secondary pull-md-left text-sm clearfix">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Shop Plans</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/members.php">Members</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/your-health.php">Your Health</a>
                    </li>
                </ul>
                <form class="form-search form-inline pull-xs-right hidden-sm-down clearfix">
                    <label class="sr-only">Search</label>
                    <input class="form-control" type="text" placeholder="Search">
                    <button class="btn-search" type="submit">
                        <span class="sr-only">Submit</span>
                        <svg till="#FFFFFF" width="24" height="24"><use xlink:href="#shape-search"></use></svg>
                    </button>
                </form>
            </div>
        </nav>

        <!-- Navbary-Tertiary -->
        <div class="navbar-tertiary container">
            <nav class="navbar pull-md-right clearfix" role="navigation">
                <ul class="nav nav-tertiary text-xs">
                    <li class="nav-item active">
                        <a class="nav-link shape-inline" href="#">
                            <svg class="hidden-sm-down" width="18" height="18"><use xlink:href="#shape-user"></use></svg>
                            <span>Sign In</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link shape-inline" href="#">
                            <svg class="hidden-sm-down" width="18" height="18"><use xlink:href="#shape-find-dentist"></use></svg>
                            <span>Find A Dentist</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link shape-inline" href="/contact.php">
                            <svg class="hidden-sm-down" width="18" height="18"><use xlink:href="#shape-user"></use></svg>
                            <span>Contact</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

    <aside class="searchbar collapse hidden-md-up" data-click-group="collapse-header" id="collapseSearch">
        <div class="container">
            <form class="form-search form-inline">
                <label class="sr-only">Search</label>
                <input class="form-control" type="text" placeholder="Search">
                <button class="btn-search" type="submit">
                    <span class="sr-only">Submit</span>
                    <svg fill="#FFFFFF" width="24" height="24"><use xlink:href="#shape-search"></use></svg>
                </button>
            </form>
        </div>
    </aside>
</header>

<main id="content" class="content">
