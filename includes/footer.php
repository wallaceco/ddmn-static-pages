        </main>

        <footer class="site-footer">
        	<div class="container">
                <div class="row flex-items-md-top">
                    <div class="col-xs-12 col-md-6 flex-order-md-first">
                        <ul class="nav nav-footer-primary">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Blog</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Contact</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Understanding Your Dental Benefits</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">deltadental.com</a>
                            </li>
                        </ul>
    				</div>
                    <div class="col-xs-12 col-md-6 flex-order-xs-first">
                        <aside class="newsletter">
                            <div class="h5 text-xs-center text-md-left">Sign up for News &amp; Updates</div>
                            <form class="form-news form-inline">
                                <svg class="hidden-sm-down" fill="#00AEC7" width="25" height="25"><use xlink:href="#shape-email"></use></svg>
                                <label class="sr-only">Email Address</label>
                                <input class="form-control" type="text" placeholder="Email Address">
                                <button class="btn btn-news btn-info text-xs-center" type="submit">
                                    <span class="hiddden-sm-down">Submit</span>
                                    <svg class="hidden-md-up m-x-auto" fill="#FFFFFF" width="25" height="25"><use xlink:href="#shape-email"></use></svg>
                                </button>
                            </form>
                        </aside>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="nav nav-footer-secondary" role="navigation">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Privacy Policies</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">HIPPA Privacy Notice</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Website Security</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Careers</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Transparency in Coverage</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row flex-items-xs-center flex-items-md-left flex-items-md-bottom">
                    <div class="col-xs-12 col-md-8 flex-order-md-first">
                        <div class="media">
                            <div class="media-left media-middle hidden-xs-down">
                                <svg class="" width="41" height="34"><use xlink:href="#shape-logo-mark"></use></svg>
                            </div>
                            <div class="media-body">
                                <div class="text-legal text-xs-center text-sm-left">&copy; 2016 Delta Dental of Minnesota and its affiliates. All rights reserved.<br /> Delta Dental of Minnesota is an authorized licensee of the Delta Dental Plans Association of Oak Brook, Illinois.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-8 col-sm-6 col-md-4 flex-order-xs-first">
                        <ul class="row flex-items-xs-middle nav nav-social text-xs-center" role="navigation">
                            <li class="col-xs nav-item">
                                <a class="nav-link" href="#">
                                    <span class="sr-only">Facebook</span>
                                    <svg width="11" height="23"><use xlink:href="#shape-facebook"></use></svg>
                                </a>
                            </li>
                            <li class="col-xs nav-item">
                                <a class="nav-link" href="#">
                                    <span class="sr-only">Twitter</span>
                                    <svg width="22" height="18"><use xlink:href="#shape-twitter"></use></svg>
                                </a>
                            </li>
                            <li class="col-xs nav-item">
                                <a class="nav-link" href="#">
                                    <span class="sr-only">YouTube</span>
                                    <svg width="22" height="27"><use xlink:href="#shape-youtube"></use></svg>
                                </a>
                            </li>
                            <li class="col-xs nav-item">
                                <a class="nav-link" href="#">
                                    <span class="sr-only">Instagram</span>
                                    <svg width="26" height="26"><use xlink:href="#shape-instagram"></use></svg>
                                </a>
                            </li>
                            <li class="col-xs nav-item">
                                <a class="nav-link" href="#">
                                    <span class="sr-only">LinkedIn</span>
                                    <svg width="17" height="17"><use xlink:href="#shape-linkedin"></use></svg>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <span class="menu-mask" data-click-target="mainNav" data-click-bodyClass="mobile-menu-is-open"></span>


        <!-- JAVASCRIPT -->
        <script type='text/javascript' src='http://www.deltadentalmn.org/wp-content/themes/deltamn/library/js/libs/modernizr.custom.min.js'></script>
        <script type='text/javascript' src='http://www.deltadentalmn.org/wp-includes/js/jquery/jquery.js'></script>
        <script type='text/javascript' src='http://www.deltadentalmn.org/wp-includes/js/jquery/jquery-migrate.min.js'></script>
        <script type="text/javascript" src="/build/js/production.js"></script>
    </body>
</html>
