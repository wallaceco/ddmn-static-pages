<?php include("includes/header.php"); ?>

<!-- Jumbotron -->
<section class="jumbotron jumbotron-fluid jumbotron-overlay jumbotron-under-nav bg-cover">
    <figure class="bg-cover__img">
        <img class="jumbotron-img" alt="FPO" src="/build/images/img-employers.jpg" />
    </figure>

    <div class="jumbotron-overlay__bd">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-10 col-lg-8">
                    <h6 class="jumbotron-subtitle h6">Employers</h6>
                    <h1 class="jumbotron-title display-1">Easily Manage <br /><span class="font-weight-bold">Your Employees' Benefits</span></h1>
                    <a class="btn btn-info" href="#">Access Employer Portal</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Media Block -->
<div class="container">
    <div class="media media-sm-stack page-intro">
        <div class="media-left media-middle page-intro__media">
            <img class="page-intro__img" src="/build/images/svgs/dd-people.svg" alt="People illustration" />
        </div>
        <div class="media-body page-intro__bd">
            <h3 class="media-heading display-2 text-uppercase text-primary">Oral Health Matters to Your Business</h3>
            <p class="lead">Healthier employees are more productive employees. Thank you to the Minnesota and North Dakota employers that trust Delta Dental of Minnesota to keep their employees healthy.</p>
        </div>
    </div>
</div>

<!-- Cards -->
<div class="container">
    <section class="card-deck-wrapper">
        <div class="card-deck">
            <div class="card card-rounded card-secondary card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/users.svg" />
                    <div class="card-subtitle h6">Manage Your Group</div>
                    <div class="card-title h3">We'll Help You Manage Your Group</div>
                    <p class="card-text p-lg">Access commonly used group forms and enrollment tools.</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
            <div class="card card-rounded card-support card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/tools.svg" />
                    <div class="card-subtitle h6">Employer Health Tools</div>
                    <div class="card-title h3">Health Resources for Your Employees</div>
                    <p class="card-text p-lg">Optimal oral health truly contributes to better overall health for your employees. Access resources to help keep your employees healthy. </p>
                    <a class="btn btn-card" href="#">View Resources</a>
                </div>
            </div>
        </div>
        <div class="card-deck">
            <div class="card card-rounded card-info card-inverse card-bg-img">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/question-mark.svg" />
                    <div class="card-subtitle h6">Dental Insurance 101</div>
                    <div class="card-title h3">Need Help Understanding Your Insurance?</div>
                    <p class="card-text p-lg">We are here to help.</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
            <div class="card card-rounded card-primary card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/trophy.svg" />
                    <div class="card-subtitle h6">Delta Dental Difference</div>
                    <div class="card-title h3">Health Resources for Employers</div>
                    <p class="card-text p-lg">Learn why more employers trust their employees' smiles to Delta Dental.</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Fluid Card -->
<div class="container-fluid card card-fluid card-inverse card-secondary card-bg-pattern">
    <div class="row">
        <div class="col-xs-12 col-md-6 bg-cover--after-sm">
            <figure class="bg-cover__img">
                <img class="" alt="FPO" src="/build/images/img-home-cta.jpg" />
            </figure>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="card-block">
                <img class="card-icon card-icon-overlay" alt="FPO" src="/build/images/svgs/briefcase-with-tooth.svg" />
                <div class="card-subtitle h6">Shop Plans</div>
                <div class="card-title h3">Do you have part-time, seasonal or retiring employees in need of a dental plan?</div>
                <p class="card-text p-lg">We have individual and family plans available.</p>
                <a class="btn btn-card" href="#">Shop Plans Now</a>
            </div>
        </div>
    </div>
</div>

<?php include("includes/footer.php"); ?>
