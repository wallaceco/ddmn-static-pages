<?php include("includes/header.php"); ?>

<!-- Jumbotron -->
<section class="jumbotron jumbotron-fluid jumbotron-overlay jumbotron-under-nav bg-cover">
    <figure class="bg-cover__img">
        <img class="jumbotron-img" alt="FPO" src="/build/images/img-community.jpg" />
    </figure>

    <div class="jumbotron-overlay__bd">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-10 col-lg-8">
                    <h1 class="jumbotron-title display-1">Commitment to the Community</h1>
                    <p class="p-lg">Delta Dental of Minnesota Foundation and Community Benefit is Minnesota's leader to improve oral health by providing support, resources and funding to increase access to care. Our impact is statewide as we work to build innovative leadership and partnerships in the oral health, health and public sectors of our community.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Circle Illustrations -->
<div class="container m-t-5 m-b-5">
    <div class="row card-step-list">
        <div class="col-xs-12 col-md-3">
            <div class="card card-step">
                <div class="card-step-circle card-inverse card-info bg-pattern-circles m-x-auto">
                    <svg class="m-x-auto" width="61" height="84"><use xlink:href="#shape-tooth-frown"></use></svg>
                </div>
                <div class="card-block text-xs-center">
                    <p class="card-text text-sm text-info font-weight-normal">Oral disease is a problem.</p>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-3">
            <div class="card card-step">
                <div class="card-step-circle card-inverse card-support bg-pattern-circles m-x-auto">
                    <svg class="m-x-auto" width="51" height="84"><use xlink:href="#shape-lightbulb"></use></svg>
                </div>
                <div class="card-block text-xs-center">
                    <p class="card-text text-sm text-support font-weight-normal">Not enough people know it's a problem.</p>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-3">
            <div class="card card-step">
                <div class="card-step-circle card-inverse card-secondary bg-pattern-circles m-x-auto">
                    <svg class="m-x-auto" width="59" height="84"><use xlink:href="#shape-hand"></use></svg>
                </div>
                <div class="card-block text-xs-center">
                    <p class="card-text text-sm text-secondary font-weight-normal">Too few people can access care or treatment.</p>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-3">
            <div class="card card-step">
                <div class="card-step-circle card-inverse card-primary bg-pattern-circles m-x-auto">
                    <svg class="m-x-auto" width="88" height="84"><use xlink:href="#shape-heart-with-tooth"></use></svg>
                </div>
                <div class="card-block text-xs-center">
                    <p class="card-text text-sm text-primary font-weight-normal">It's preventable.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Media Block -->
<div class="container m-t-5 m-b-5">
    <div class="media media-sm-stack">
        <div class="media-left media-middle page-intro__media">
            <img class="page-intro__img" src="/build/images/svgs/dd-mn-tooth.svg" alt="MN Community illustration" />
        </div>
        <div class="media-body page-intro__bd">
            <h3 class="media-heading display-2 text-uppercase text-primary">How Do We Help?</h3>
            <p><span class="font-weight-bold">Delta Dental of Minnesota Community Foundation and Community Benefits do this by focusing on:</span></p>
            <ul>
                <li>Workforce Development</li>
                <li>Access to Care (Safety Net)</li>
                <li>Prevention &amp; Education</li>
            </ul>
            <a class="btn btn-info" href="#">See where we provide coverage</a>
        </div>
    </div>
</div>

<!-- Cards -->
<div class="container">
    <section class="card-deck-wrapper">
        <div class="card-deck">
            <div class="card card-rounded card-support card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/shaking-hands.svg" />
                    <div class="card-subtitle h6">How to Partner</div>
                    <div class="card-title h3">Delta Dental of Minnesota Foundation</div>
                    <p class="card-text p-lg">This foundation supports the mission of improving the health through oral health of the people in Minnesota and advancing the science of oral health in Minnesota.</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
            <div class="card card-rounded card-info card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/shaking-hands.svg" />
                    <div class="card-subtitle h6">How to Partner</div>
                    <div class="card-title h3">Responsive Grant Program</div>
                    <p class="card-text p-lg">Our goal is to work hand-in-hand with public and nonprofit organizations to identify and implement solutions that address specific problems.</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Fluid Card -->
<div class="container-fluid card card-fluid card-inverse card-primary card-bg-pattern" id="shop-plans">
    <div class="row">
        <div class="col-xs-12 col-md-6 bg-cover--after-sm">
            <figure class="bg-cover__img">
                <img class="" alt="FPO" src="/build/images/img-community-cta.jpg" />
            </figure>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="card-block">
                <img class="card-icon card-icon-logo" src="/build/images/svgs/logo-primary-white.svg" alt="Delta Dental logo" />
                <div class="card-subtitle h6">About Us</div>
                <div class="card-title h3">Learn More About our Commitment to the Community</div>
                <p class="card-text p-lg">For more about our history, leadership, annual reports and how to contact us.</p>
                <a class="btn btn-card" href="#">Contact Us</a>
            </div>
        </div>
    </div>
</div>

<!-- Blog Preview Cards -->
<section class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <article class="card card-post-preview">
                <img class="card-img-top img-fluid img-rounded" src="/build/images/fpo-thumb1.jpg" alt="FPO blog post thumbnail" />
                <div class="card-block">
                    <div class="card-subtitle h6 shape-inline">
                        <svg width="16" height="19"><use xlink:href="#shape-bookmark"></use></svg>
                        <span>August 8, 2016</span>
                    </div>
                    <a href="#" class="card-title h3">Sealing the Perfect Smile!</a>
                    <p class="card-text text-sm">Delta Dental is funding an initiative to provide children with dental sealants.</p>
                    <footer class="">
                        <a class="link-more text-sm font-weight-medium shape-inline" href="#">
                            <span>Read More</span>
                            <svg width="13" height="13"><use xlink:href="#shape-circle-arrow-right"></use></svg>
                        </a>
                    </footer>
                </div>
            </article>
        </div>
        <div class="col-xs-12 col-sm-4">
            <article class="card card-post-preview">
                <img class="card-img-top img-fluid img-rounded" src="/build/images/fpo-thumb2.jpg" alt="FPO blog post thumbnail" />
                <div class="card-block">
                    <div class="card-subtitle h6 shape-inline">
                        <svg width="16" height="19"><use xlink:href="#shape-bookmark"></use></svg>
                        <span>July 30, 2016</span>
                    </div>
                    <a href="#" class="card-title h3">Major Life Changes &amp; Your Dental Insurance</a>
                    <p class="card-text text-sm">What you need to know about how major changes in your life affect your dental care.</p>
                    <footer class="">
                        <a class="link-more text-sm font-weight-medium shape-inline" href="#">
                            <span>Read More</span>
                            <svg width="13" height="13"><use xlink:href="#shape-circle-arrow-right"></use></svg>
                        </a>
                    </footer>
                </div>
            </article>
        </div>
        <div class="col-xs-12 col-sm-4">
            <article class="card card-post-preview">
                <img class="card-img-top img-fluid img-rounded" src="/build/images/fpo-thumb3.jpg" alt="FPO blog post thumbnail" />
                <div class="card-block">
                    <div class="card-subtitle h6 shape-inline">
                        <svg width="16" height="19"><use xlink:href="#shape-bookmark"></use></svg>
                        <span>July 1, 2016</span>
                    </div>
                    <a href="#" class="card-title h3">Don't Toss the Floss</a>
                    <p class="card-text text-sm">Recent studies have questioned the necessity of flossing, but here’s the truth.</p>
                    <footer class="">
                        <a class="link-more text-sm font-weight-medium shape-inline" href="#">
                            <span>Read More</span>
                            <svg width="13" height="13"><use xlink:href="#shape-circle-arrow-right"></use></svg>
                        </a>
                    </footer>
                </div>
            </article>
        </div>
    </div>
    <div class="hr-btn clearfix m-b-4">
        <a class="btn btn-info pull-xs-right" href="#">View All</a>
    </div>
</section>

<?php include("includes/footer.php"); ?>
