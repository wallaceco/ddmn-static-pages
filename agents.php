<?php include("includes/header.php"); ?>

<!-- Jumbotron -->
<section class="jumbotron jumbotron-fluid jumbotron-overlay jumbotron-under-nav bg-cover">
    <figure class="bg-cover__img">
        <img class="jumbotron-img" alt="FPO" src="/build/images/img-agents.jpg" />
    </figure>

    <div class="jumbotron-overlay__bd">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-10 col-lg-8">
                    <h6 class="jumbotron-subtitle h6">Agents</h6>
                    <h1 class="jumbotron-title display-1">Introducing Our New <br /><span class="font-weight-bold">Broker Rate Calculator</span></h1>
                    <div class="btn-inline">
                        <a class="btn btn-info" href="#">Try It Out</a>
                        <a class="btn btn-outline-info shape-inline" href="#">
                            <svg fill="#00ADCB" width="18" height="18"><use xlink:href="#shape-play-icon"></use></svg>
                            <span>Take the Tour</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Media Block -->
<div class="container">
    <div class="media media-sm-stack page-intro">
        <div class="media-left media-middle page-intro__media">
            <img class="page-intro__img" src="/build/images/svgs/dd-shake.svg" alt="Shaking Hands illustration" />
        </div>
        <div class="media-body page-intro__bd">
            <h3 class="media-heading display-2 text-uppercase text-primary">Your Trusted Partner in Dental Benefits</h3>
            <p><span class="font-weight-bold">More Minnesota agents trust their clients' oral health to Delta Dental because we offer:</span></p>
            <ul>
                <li>Market-level commissions</li>
                <li>Outstanding customer service</li>
                <li>Dedicated sales, implementation and support teams</li>
                <li>The country's largest dental-provider network</li>
                <li>The industry's most trusted brand</li>
            </ul>
        </div>
    </div>
</div>

<!-- Cards -->
<div class="container">
    <section class="card-deck-wrapper">
        <div class="card-deck">
            <div class="card card-rounded card-support card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/shaking-hands.svg" />
                    <div class="card-subtitle h6">Get Appointed</div>
                    <div class="card-title h3">Get Appointed with Delta Dental of Minnesota</div>
                    <p class="card-text p-lg">Interested in becoming a Delta Dental agent?</p>
                    <a class="btn btn-card" href="#">Learn More</a>
                </div>
            </div>
            <div class="card card-rounded card-secondary card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/book.svg" />
                    <div class="card-subtitle h6">Assist Your Clients</div>
                    <div class="card-title h3">Are You an Agent Shopping for Your Clients?</div>
                    <p class="card-text p-lg">Access agent forms and resources to help assist your employer groups and members</p>
                    <a class="btn btn-card" href="#">View Resources</a>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include("includes/footer.php"); ?>
