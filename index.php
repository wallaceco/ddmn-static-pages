<?php include("includes/header.php"); ?>

<!-- Jumbotron -->
<section class="jumbotron jumbotron-fluid jumbotron-overlay jumbotron-under-nav bg-cover">
    <figure class="bg-cover__img">
        <img class="jumbotron-img" alt="FPO" src="/build/images/img-home.jpg" />
    </figure>

    <div class="jumbotron-overlay__bd jumbotron-overlay__bd-lg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-10 col-lg-8">
                    <h6 class="jumbotron-subtitle h6">Shop Delta Dental</h6>
                    <h1 class="jumbotron-title display-1">
                        Your Smile is Powerful. <br /><span class="font-weight-bold">It Deserves Delta Dental.</span>
                    </h1>
                    <p class="p-lg">We have a dental plan that fits your needs and budget.</p>
                    <a class="btn btn-info" href="#">Shop Plans Now</a>
                </div>
            </div>
        </div>
    </div>

    <div class="scroll-to">
        <div class="btn-scroll-to m-x-auto flex-vh-center" data-scroll-to="shop-plans">
            <svg width="19" height="13"><use xlink:href="#shape-carrot-down"></use></svg>
        </div>
    </div>
</section>

<!-- Cards -->
<div class="container">
    <section class="card-deck-wrapper">
        <div class="card-deck">
            <a href="#" class="card card-rounded card-support card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/house-with-heart.svg" />
                    <div class="card-subtitle h6">Community Impact</div>
                    <div class="card-title h3">Improving Oral Health in Our Community</div>
                    <p class="card-text p-lg">Delta Dental of Minnesota Foundation and Community Benefit support oral health education initiatives to improve the health of our community.</p>
                    <div class="btn btn-card" href="#">Learn More</div>
                </div>
            </a>
            <a href="#" class="card card-rounded card-secondary card-inverse">
                <div class="card-block">
                    <img class="card-icon" alt="FPO" src="/build/images/svgs/heart-with-tooth.svg" />
                    <div class="card-subtitle h6">Your Health</div>
                    <div class="card-title h3">A Healthy Mouth is Part of a Healthy Life</div>
                    <p class="card-text p-lg">Take advantage of a variety of oral health tips, resources and information to improve your oral and overall health.</p>
                    <div class="btn btn-card" href="#">Learn More</div>
                </div>
            </a>
        </div>
    </section>
</div>

<!-- Fluid Card -->
<div class="container-fluid card card-fluid card-inverse card-primary card-bg-pattern" id="shop-plans">
    <div class="row">
        <div class="col-xs-12 col-md-6 bg-cover--after-sm">
            <figure class="bg-cover__img">
                <img class="" alt="FPO" src="/build/images/img-home-cta.jpg" />
            </figure>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="card-block">
                <img class="card-icon card-icon-logo" src="/build/images/svgs/logo-primary-white.svg" alt="Delta Dental logo" />
                <div class="card-subtitle h6">About Us</div>
                <div class="card-title h3">We're dedicated to help you improve your health through oral health.</div>
                <p class="card-text p-lg">Delta Dental of Minnesota is proud to be the largest regional provider of dental benefits with 6,500 Minnesota and North Dakota-based purchasing groups and 8.3 million members nationwide. See why more people trust their dental benefits to us!</p>
                <a class="btn btn-card" href="#">Shop Plans Now</a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="card card-xs-fluid card-strip card-rounded card-inverse card-info card-bg-img">
        <div class="card-block">
            <img class="card-icon" alt="FPO" src="/build/images/svgs/question-mark.svg" />
            <div class="card-title h3">Need Help Understanding Your Dental Plan?</div>
            <a class="btn btn-card" href="#">Learn More</a>
        </div>
    </div>
</div>

<?php include("includes/footer.php"); ?>
